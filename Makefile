LIB=/include/raylib/src/
INCLUDE=/include/raylib/src/

export LD_LIBRARY_PATH=$(LIB)
extras=-Wall -O0 				#-static
# extras="-Wall -static"

all: plot run

plot: plot.c
	gcc $(extras) plot.c -o plot -ggdb -L$(LIB) -I$(INCLUDE) -lraylib -lm

subplot: subplot.c
	gcc $(extras) subplot.c -o subplot -ggdb -L$(LIB) -I$(INCLUDE) -lraylib -lm

run: plot subplot
	./plot & ./subplot
