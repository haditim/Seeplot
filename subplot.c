#include <math.h>
#include <raylib.h>
#include <stdio.h>

#define MAX_POINTS 5000
#define VIEWPORT_WIDTH 1000
#define VIEWPORT_HEIGHT 600
#define BORDER_THICKNESS 3
#define LINE_THICKNESS 5
#define INNER_MARGIN_PERCENT 5

typedef struct {
    float x;
    float y;
    bool isLeft;
} DataPoint;

// Subplot data blueprint
typedef struct {
    Vector2 topl_abs;
    Vector2 topl_rel;
    Vector2 botr_abs;
    Vector2 botr_rel;
    char *x_label;
    char *y_label;
} Subplot;

DataPoint data[MAX_POINTS];

Rectangle zoomRect = {0};
bool isZooming = false;
Rectangle resetButton = {700, 10, 80, 30};
Vector2 subplot_margin = {20, 20};

extern void rlViewport(int x, int y, int width, int height);
void generateData()
{
    for (int i = 0; i < MAX_POINTS; i++) {
        data[i].x = (float)i;
        data[i].isLeft = i < MAX_POINTS / 2;
        data[i].y = data[i].isLeft ? sin(data[i].x * DEG2RAD)
                                   : cos(data[i].x * DEG2RAD);
    }
}

static Vector2 get_abs_point(Vector2 point, Subplot subplot)
{
    float y_range_rel = ((float)subplot.botr_rel.y - subplot.topl_rel.y);
    float x_range_rel = ((float)subplot.botr_rel.x - subplot.topl_rel.x);
    float y_range_abs = ((float)subplot.botr_abs.y - subplot.topl_abs.y);
    float x_range_abs = ((float)subplot.botr_abs.x - subplot.topl_abs.x);
    return (Vector2){
        .x = ((point.x - subplot.topl_rel.x) / x_range_rel) * x_range_abs +
             subplot.topl_abs.x,
        .y = ((point.y - subplot.topl_rel.y) / y_range_rel) * y_range_abs +
             subplot.topl_abs.y,
    };
}

static void set_inner_margin(Subplot *subplot)
{
    subplot->topl_rel.x -= (subplot->botr_rel.x - subplot->topl_rel.x) *
                           INNER_MARGIN_PERCENT / 100;
    subplot->botr_rel.x += (subplot->botr_rel.x - subplot->topl_rel.x) *
                           INNER_MARGIN_PERCENT / 100;
    subplot->topl_rel.y -= (subplot->botr_rel.y - subplot->topl_rel.y) *
                           INNER_MARGIN_PERCENT / 100;
    subplot->botr_rel.y += (subplot->botr_rel.y - subplot->topl_rel.y) *
                           INNER_MARGIN_PERCENT / 100;
}

static void draw_subplot(Subplot subplot)
{
    Rectangle left_rect = {subplot.topl_abs.x, subplot.topl_abs.y,
                           subplot.botr_abs.x - subplot.topl_abs.x,
                           subplot.botr_abs.y - subplot.topl_abs.y};
    DrawRectangleLinesEx(left_rect, BORDER_THICKNESS, LIGHTGRAY);
    DrawText(subplot.x_label,
             (subplot.botr_abs.x - subplot.topl_abs.x) / 2 + subplot.topl_abs.x,
             subplot.botr_abs.y, 20, BLACK);
    DrawTextPro(GetFontDefault(), subplot.y_label,
                (Vector2){subplot.topl_abs.x - subplot_margin.x,
                          subplot.botr_abs.y / 2},
                (Vector2){0, 0}, -90, 20, 1, BLACK);
}

int main()
{
    int config_flags = FLAG_WINDOW_MAXIMIZED | FLAG_WINDOW_RESIZABLE |
                       FLAG_MSAA_4X_HINT | FLAG_WINDOW_RESIZABLE |
                       FLAG_WINDOW_HIGHDPI;
    SetConfigFlags(config_flags);
    InitWindow(1000, 600, "Zoomable Raylib Data Plotting");
    SetTargetFPS(30);
    int screenWidth = GetScreenWidth();
    int screenHeight = GetScreenHeight();
    generateData();
    // Go through data to check maxima for each subplot
    Subplot left = {.topl_abs = {subplot_margin.x / 2, subplot_margin.y / 2},
                    .topl_rel = {+INFINITY, +INFINITY},
                    .botr_abs = {(float)screenWidth / 2 - subplot_margin.x / 2,
                                 screenHeight - subplot_margin.y / 2},
                    .botr_rel = {-INFINITY, -INFINITY},
                    .x_label = "time",
                    .y_label = "intensity"};

    Subplot right = {.topl_abs = {(float)screenWidth / 2 + subplot_margin.x / 2,
                                  subplot_margin.y / 2},
                     .topl_rel = {+INFINITY, +INFINITY},
                     .botr_abs = {screenWidth - subplot_margin.x / 2,
                                  screenHeight - subplot_margin.y / 2},
                     .botr_rel = {-INFINITY, -INFINITY},
                     .x_label = "time",
                     .y_label = "intensity"};

    for (int i = 0; i < MAX_POINTS; i++) {
        if (data[i].isLeft) {
            if (left.topl_rel.x > data[i].x)
                left.topl_rel.x = data[i].x;
            if (left.topl_rel.y > data[i].y)
                left.topl_rel.y = data[i].y;
            if (left.botr_rel.x < data[i].x)
                left.botr_rel.x = data[i].x;
            if (left.botr_rel.y < data[i].y)
                left.botr_rel.y = data[i].y;
        }
        if (!data[i].isLeft) {
            if (right.topl_rel.x > data[i].x)
                right.topl_rel.x = data[i].x;
            if (right.topl_rel.y > data[i].y)
                right.topl_rel.y = data[i].y;
            if (right.botr_rel.x < data[i].x)
                right.botr_rel.x = data[i].x;
            if (right.botr_rel.y < data[i].y)
                right.botr_rel.y = data[i].y;
        }
    }

    set_inner_margin(&left);
    set_inner_margin(&right);

    while (!WindowShouldClose()) {
        Vector2 scale = GetWindowScaleDPI();

        rlViewport(0, 0, VIEWPORT_WIDTH * scale.x, VIEWPORT_HEIGHT * scale.y);

        if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
            zoomRect.x = GetMouseX();
            zoomRect.y = GetMouseY();
            isZooming = true;
        }

        // Not working yet
        if (isZooming) {
            zoomRect.width = GetMouseX() - zoomRect.x;
            zoomRect.height = GetMouseY() - zoomRect.y;
            printf("%d, %d\n", GetMouseX(), GetMouseY());

            if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {
                isZooming = false;

                // Perform zooming action here
                // Update subplot data based on zoomRect
            }
        }

        BeginDrawing();

        ClearBackground(RAYWHITE);

        draw_subplot(left);
        draw_subplot(right);

        for (int i = 0; i < MAX_POINTS - 1; i++) {
            if (data[i].isLeft) {
                Vector2 point1 =
                    get_abs_point((Vector2){data[i].x, data[i].y}, left);
                Vector2 point2 = get_abs_point(
                    (Vector2){data[i + 1].x, data[i + 1].y}, left);
                DrawLineEx(point1, point2, LINE_THICKNESS, RED);
            } else {
                Vector2 point1 =
                    get_abs_point((Vector2){data[i].x, data[i].y}, right);
                Vector2 point2 = get_abs_point(
                    (Vector2){data[i + 1].x, data[i + 1].y}, right);
                DrawLineEx(point1, point2, LINE_THICKNESS, BLUE);
            }
        }

        if (isZooming) {
            DrawRectangleLines(zoomRect.x, zoomRect.y, zoomRect.width,
                               zoomRect.height, BLACK);
            DrawText("Zooming...", zoomRect.x + 10, zoomRect.y + 10, 20, BLACK);
            SetMouseCursor(MOUSE_CURSOR_RESIZE_ALL);
        } else {
            SetMouseCursor(MOUSE_CURSOR_DEFAULT);
        }
        DrawRectangleLinesEx(resetButton, 2, BLACK);
        DrawText("Reset Zoom", resetButton.x + 10, resetButton.y + 8, 20,
                 BLACK);

        EndDrawing();

        if (CheckCollisionPointRec(GetMousePosition(), resetButton)) {
            SetMouseCursor(MOUSE_CURSOR_POINTING_HAND);

            if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
                // Reset zoom action here
                // You can add code to reset the zoom functionality
            }
        } else {
            SetMouseCursor(MOUSE_CURSOR_DEFAULT);
        }
    }

    CloseWindow();
    return 0;
}
