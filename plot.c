#include <math.h>
#include <raylib.h>

#define MAX_POINTS 5000
#define VIEWPORT_WIDTH 1000
#define VIEWPORT_HEIGHT 600
#define BORDER_THICKNESS 3
#define LINE_THICKNESS 5
#define INNER_MARGIN_PERCENT 5

typedef struct {
    Vector2 topl_abs;
    Vector2 topl_rel;
    Vector2 botr_abs;
    Vector2 botr_rel;
    char *x_label;
    char *y_label;
} Plot;

Vector2 data[MAX_POINTS];

Vector2 subplot_margin = {20, 20};

extern void rlViewport(int x, int y, int width, int height);
void generateData()
{
    for (int i = 0; i < MAX_POINTS; i++) {
        data[i].x = (float)i;
        data[i].y = sin(data[i].x * DEG2RAD);
    }
}

static Vector2 get_abs_point(Vector2 point, Plot plot)
{
    float y_range_rel = ((float)plot.botr_rel.y - plot.topl_rel.y);
    float x_range_rel = ((float)plot.botr_rel.x - plot.topl_rel.x);
    float y_range_abs = ((float)plot.botr_abs.y - plot.topl_abs.y);
    float x_range_abs = ((float)plot.botr_abs.x - plot.topl_abs.x);
    return (Vector2){
        .x = ((point.x - plot.topl_rel.x) / x_range_rel) * x_range_abs +
             plot.topl_abs.x,
        .y = ((point.y - plot.topl_rel.y) / y_range_rel) * y_range_abs +
             plot.topl_abs.y,
    };
}

static void set_inner_margin(Plot *plot)
{
    plot->topl_rel.x -=
        (plot->botr_rel.x - plot->topl_rel.x) * INNER_MARGIN_PERCENT / 100;
    plot->botr_rel.x +=
        (plot->botr_rel.x - plot->topl_rel.x) * INNER_MARGIN_PERCENT / 100;
    plot->topl_rel.y -=
        (plot->botr_rel.y - plot->topl_rel.y) * INNER_MARGIN_PERCENT / 100;
    plot->botr_rel.y +=
        (plot->botr_rel.y - plot->topl_rel.y) * INNER_MARGIN_PERCENT / 100;
}

static void draw_plot(Plot plot)
{
    Rectangle rect = {plot.topl_abs.x, plot.topl_abs.y,
                      plot.botr_abs.x - plot.topl_abs.x,
                      plot.botr_abs.y - plot.topl_abs.y};
    DrawRectangleLinesEx(rect, BORDER_THICKNESS, LIGHTGRAY);
    DrawText(plot.x_label,
             (plot.botr_abs.x - plot.topl_abs.x) / 2 + plot.topl_abs.x,
             plot.botr_abs.y, 20, BLACK);
    DrawTextPro(
        GetFontDefault(), plot.y_label,
        (Vector2){plot.topl_abs.x - subplot_margin.x, plot.botr_abs.y / 2},
        (Vector2){0, 0}, -90, 20, 1, BLACK);
}

int main()
{
    int config_flags = FLAG_WINDOW_MAXIMIZED | FLAG_WINDOW_RESIZABLE |
                       FLAG_MSAA_4X_HINT | FLAG_WINDOW_RESIZABLE |
                       FLAG_WINDOW_HIGHDPI;
    SetConfigFlags(config_flags);
    InitWindow(1000, 600, "Zoomable Raylib Data Plotting");
    SetTargetFPS(30);
    int screenWidth = GetScreenWidth();
    int screenHeight = GetScreenHeight();
    generateData();
    // Go through data to check maxima for each subplot
    Plot plot = {.topl_abs = {subplot_margin.x / 2, subplot_margin.y / 2},
                 .topl_rel = {+INFINITY, +INFINITY},
                 .botr_abs = {(float)screenWidth - subplot_margin.x / 2,
                              screenHeight - subplot_margin.y / 2},
                 .botr_rel = {-INFINITY, -INFINITY},
                 .x_label = "time",
                 .y_label = "intensity"};

    for (int i = 0; i < MAX_POINTS; i++) {
        if (plot.topl_rel.x > data[i].x)
            plot.topl_rel.x = data[i].x;
        if (plot.topl_rel.y > data[i].y)
            plot.topl_rel.y = data[i].y;
        if (plot.botr_rel.x < data[i].x)
            plot.botr_rel.x = data[i].x;
        if (plot.botr_rel.y < data[i].y)
            plot.botr_rel.y = data[i].y;
    }

    set_inner_margin(&plot);

    while (!WindowShouldClose()) {
        // If display has fractional scaling
        Vector2 scale = GetWindowScaleDPI();
        rlViewport(0, 0, VIEWPORT_WIDTH * scale.x, VIEWPORT_HEIGHT * scale.y);

        BeginDrawing();

        ClearBackground(RAYWHITE);

        draw_plot(plot);

        for (int i = 0; i < MAX_POINTS - 1; i++) {
            Vector2 point1 =
                get_abs_point((Vector2){data[i].x, data[i].y}, plot);
            Vector2 point2 =
                get_abs_point((Vector2){data[i + 1].x, data[i + 1].y}, plot);
            DrawLineEx(point1, point2, LINE_THICKNESS, RED);
        }

        EndDrawing();
    }

    CloseWindow();
    return 0;
}
